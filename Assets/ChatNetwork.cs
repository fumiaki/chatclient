using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;


public class ChatNetwork:Network {
	
	public static bool CheckWWWError(WWW www){
		if (!string.IsNullOrEmpty(www.error)){
			Debug.LogWarning("WWW error : " + www.url + " : " + www.error);
			return true;
		}
		return false;
	}

	public void Login(System.Action<string> callback, System.Action<string> errorCallback) {
		BaseNetworkCall call = new BaseNetworkCall(BaseNetworkCall.HTTPMethod.POST, "login", callback, errorCallback);
		call.AddField("user_name", _userName);
		StartCoroutine(SendAsynchronous(call));
	}
	
	public void Rooms(System.Action<string> callback, System.Action<string> errorCallback) {
		// TODO: Programmatic way of adding GET arguments
		BaseNetworkCall call = new BaseNetworkCall(BaseNetworkCall.HTTPMethod.GET, "rooms?user_name="+_userName, callback, errorCallback);
		StartCoroutine(SendAsynchronous(call));
	}

	public void Join(string roomName, System.Action<string> callback, System.Action<string> errorCallback) {
		BaseNetworkCall call = new BaseNetworkCall(BaseNetworkCall.HTTPMethod.POST, "join", callback, errorCallback);
		call.AddField("user_name", _userName);
		call.AddField("room_name", roomName);
		StartCoroutine(SendAsynchronous(call));
	}
	
	public void Leave(System.Action<string> callback, System.Action<string> errorCallback) {
		BaseNetworkCall call = new BaseNetworkCall(BaseNetworkCall.HTTPMethod.POST, "leave", callback, errorCallback);
		call.AddField("user_name", _userName);
		StartCoroutine(SendAsynchronous(call));
	}
	
	public void Chat(string message, System.Action<string> callback, System.Action<string> errorCallback) {
		BaseNetworkCall call = new BaseNetworkCall(BaseNetworkCall.HTTPMethod.POST, "chat", callback, errorCallback);
		call.AddField("user_name", _userName);
		call.AddField("message", message);
		StartCoroutine(SendAsynchronous(call));
	}
	
	public void Poll(System.Action<string> callback, System.Action<string> errorCallback) {
		BaseNetworkCall call = new BaseNetworkCall(BaseNetworkCall.HTTPMethod.POST, "poll", callback, errorCallback);
		call.AddField("user_name", _userName);
		StartCoroutine(SendAsynchronous(call));
	}
	
	private IEnumerator SendAsynchronous(BaseNetworkCall networkCall) {
		WWW www = null;
		try{
			if (BaseNetworkCall.HTTPMethod.POST==networkCall.Method) {
				www = SendRequest(networkCall.GetUrl(), networkCall.GetForm());
			}
			else {
				www = SendRequest(networkCall.GetUrl());
			}
		}
		catch(Exception e){
			Debug.Log("Exception:" + e);
		}
			
		yield return www;
		try{
			if (!CheckWWWError(www)) {
				networkCall.HandleCallback(www.text);
			}
			else {
				networkCall.HandleCallback(www.error);
			}
			www.Dispose();
		}
		catch(Exception e2){
			networkCall.HandleCallback(www.error);
			Debug.Log("Exception:" + e2);
		}
	}
	
	protected class BaseNetworkCall{
		public enum HTTPMethod {
			GET,
			POST
		}

		protected HTTPMethod _method = HTTPMethod.GET;
		protected string _url;
		protected Hashtable _field = new Hashtable();
		protected System.Action<string> _callback = null;
		protected System.Action<string> _errorCallback = null;
		
		public BaseNetworkCall(HTTPMethod method, string url, System.Action<string> callback, System.Action<string> errorCallback){
			//force form to have a field because unity get/post issues
			_field.Add("forcepost", "true");
			_method = method;
			_url = url;
			_callback = callback;
			_errorCallback = errorCallback;
		}

		public HTTPMethod Method {
			get {
				return _method;
			}
		}
		
		public void AddField(string key, string val) {
			_field[key] = val;
		}
		
		public void AddField(string key, int val) {
			_field[key] = val;
		}
		
		public void AddField(string key, bool val) {
			_field[key]= (val?"true":"false");
		}
		
		public void AddField(string key, byte[] val) {
			_field[key]= Convert.ToBase64String(val);
		}

		public void HandleCallback(string result) {
			if (null!=_callback) {
				_callback(result);
			}
		}

		public void HandleErrorCallback(string result) {
			if (null!=_errorCallback) {
				_errorCallback(result);
			}
		}
		
		public string GetUrl() {
			return _url;
		}
		
		public WWWForm GetForm() {
			WWWForm form = new WWWForm();
			foreach (DictionaryEntry pair in _field) {
				if (pair.Value is string) {
					form.AddField(pair.Key as string, pair.Value as string);
				}
				else {
					form.AddField(pair.Key as string, (int)pair.Value);
				}
			}
			return form;
		}
	}
	

}
