﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChatClient : MonoBehaviour {
	public ChatNetwork network = null;
	public PanelRoom roomPanel = null;
	public GameObject chatPanel = null;
	public Scrollbar logScrollbar = null;
	public Text text = null;

	private string userName = "";
	private string server = "52.90.198.228:5080";
	private string message = "";


	private bool hasRooms = false;
	private List<string> rooms = null;

	private bool inRoom = false;
	private float lastPollTime = 0f;

	// Use this for initialization
	void Start () {
		rooms = new List<string>();
	}
	
	// Update is called once per frame
	void Update () {
		if (inRoom) {
			// Poll the server for new messages every 2 seconds
			float diff = Time.time - lastPollTime;
			if (2f<diff) {
				network.Poll(HandlePollResponse, HandleErrorResponse);
				lastPollTime = Time.time;
			}
		}
	}

	public void Login() {
		Debug.Log("Login "+userName);
		network.SetUserName(userName);
		network.SetServer(server);
		network.Login(HandleLoginResponse, HandleErrorResponse);
	}

	public void SetUserName(string userName) {
		Debug.Log("set user:"+userName);
		this.userName = userName;
	}

	public void SetServer(string newServer) {
		Debug.Log("set server:"+userName);
		this.server = newServer;
	}

	public void SetMessage(string message) {
		this.message = message;
	}

	public bool HasRooms() {
		return hasRooms;
	}

	public List<string> GetRooms() {
		return rooms;
	}

	public void SelectRoom(string roomName) {
		network.Join(roomName, HandleJoinResponse, HandleErrorResponse);
		roomPanel.GetComponent<Animator>().SetTrigger("CloseTrigger");


	}

	public void Leave() {
		network.Leave(HandleLeaveResponse, HandleErrorResponse);
		chatPanel.GetComponent<Animator>().SetTrigger("TriggerClose");
	}

	public void SendMessage() {
		network.Chat(message+'\n', HandleMessageResponse, HandleErrorResponse);
	}

	public void HandleLoginResponse(string result) {
		AddToLog(result);
		network.Rooms(HandleRoomsResponse, HandleErrorResponse);
		hasRooms = false;
	}

	public void HandleRoomsResponse(string result) {
		AddToLog(result);
		hasRooms = true;
		rooms = ParseRooms(result);
	}

	public void HandleJoinResponse(string result) {
		AddToLog(result);
		inRoom = true;
		chatPanel.GetComponent<Animator>().SetTrigger("TriggerOpen");
	}

	public void HandleLeaveResponse(string result) {
		AddToLog(result);
		inRoom = false;
		roomPanel.GetComponent<Animator>().SetTrigger("OpenTrigger");
	}
	
	public void HandleMessageResponse(string result) {
		AddToLog(result);
	}

	public void HandlePollResponse(string result) {
		AddToLog(result);
	}

	private void AddToLog(string log) {
		if (1<log.Length) {
			log = log.Trim('\n');
			Debug.Log(log);
			var cachedText = text.cachedTextGenerator;
			if (50<cachedText.lineCount) {
				text.text = "";
			}
			text.text += log+'\n';
			logScrollbar.value = 0f;
		}
	}

	public void HandleErrorResponse(string result) {
		Debug.Log(result);
	}

	private List<string> ParseRooms(string roomResult) {
		List<string> result = new List<string>();
		string[] lines = roomResult.Split('\n');
		// ignore first and last line, because they're not rooms
		for (int i=1; i<lines.Length-2; ++i) {
			// trim the asterix and the number of players in the room
			// for real live situation, the http and telnet protocol should just be different
			string roomName = lines[i].TrimStart(" *".ToCharArray());
			roomName = roomName.Substring(0, roomName.IndexOf('('));
			result.Add(roomName);
		}
		return result;
	}
}
