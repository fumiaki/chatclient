﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PanelRoom : MonoBehaviour {
	public GameObject buttonPrefab = null;
	public ChatClient chatClient = null;

	private bool initialized = false;
	private List<GameObject> buttons = null;

	// Use this for initialization
	void Start () {
		buttons = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!initialized) {
			if (chatClient.HasRooms()) {
				Debug.Log("init rooms");
				// TODO: recycle rather than destroy
				foreach(GameObject childButton in buttons) {
					Destroy(childButton);
				}
				buttons = new List<GameObject>();
				List<string> rooms = chatClient.GetRooms();
				foreach(string room in rooms) {
					CreateButton(room);
				}
				initialized = true;
			}
		}
	
	}

	void OnEnable() {
	}


	private void CreateButton(string name) {
		GameObject buttonObj = GameObject.Instantiate(buttonPrefab);
		Button button = buttonObj.GetComponent<Button>();
		button.transform.SetParent(gameObject.transform);
		button.transform.GetComponentsInChildren<Text>()[0].text = name;
		button.onClick.AddListener(delegate {
			chatClient.SelectRoom(name);
		});
		buttons.Add(buttonObj);
	}
}
