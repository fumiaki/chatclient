using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkSessionTable:Dictionary<string, string>{}

public class Network: MonoBehaviour {
	protected string _userName = null;
	
	private string _serverURL = null;

	private NetworkSessionTable _session_table = new NetworkSessionTable();

	
	protected void NetworkAwake () {
		_session_table["Content-Type"] = "application/x-www-form-urlencoded";
	}
	
	
	#region cookies
	 
	// and some helper functions and properties
	public NetworkSessionTable SessionCookie {
	    get { return _session_table; }
	}
	
	#endregion
	
	public void SetUserName(string userName) {
		_userName = userName;
	}
	
	public void SetServer(string remoteURL) {
		_serverURL = remoteURL;
	}
	
	
	public string GetRemoteURL() {
		return _serverURL;
	}
	


	public WWW SendRequest(string url, Dictionary<string, string> headers=null){
		WWW www = new WWW(_serverURL + "/"+url, null, headers);
		return www;
		
	}
	
	public WWW SendRequest(string url, WWWForm form, Dictionary<string, string> headers=null){
		WWW www = new WWW(_serverURL + "/"+url, form.data, headers);
		return www;
		
	}
	
	
	
}


