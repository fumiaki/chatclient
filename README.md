# README #

This is the chat client written in Unity. This client uses the REST based API that was added to the chat server.

### How do I get set up? ###

* Just get the code and compile. The code has been tested using Unity 5.1.2
* Here is the repository for the server [[https://bitbucket.org/fumiaki/chatserver]]